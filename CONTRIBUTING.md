## Contributing to CCoE Repositories

First of all, thank you for taking the time to contribute to this repository. :tada: :+1: :heart:

When contributing to CCoE repositories, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change. Please refer 
to the README,md for ownership and contact information. 

Please note we have a Code of Conduct (see below or refer to CODE_OF_CONDUCT.md), please follow it 
in all your interactions with the project.

Please include a link to this CONTRIBUTING.md in the repository README.md.

Please also define the CODE_OF_CONDUCT.md explicitly in the repository.

## Maintenance Responsibilities

The maintainers of this repository do not take the responsibility
of any bugs introduced and bugfixes and new feature are introduced at best effort.
Of course, bugfixes are welcome to be done via contribution following the Merge Request Process. 

## Merge Request Process

1. Contributions are only accepted via Merge Requests. 
2. Ensure any install or build dependencies are removed when doing a Merge Request.
3. Update the README.md with details of changes, which may include new environment 
   variables, newly exposed interfaces and API endpoints, useful file locations and 
   container parameters, etc.
4. Increase the version numbers in any examples files and the README.md to the new version that this
   Merge Request would represent. The versioning scheme we use is [SemVer](http://semver.org/).
5. You may merge the Merge Request once you have the sign-off of a repository maintainer, or if you 
   do not have permission to do that, you may request the second reviewer (maintainer) to merge it for you. 

## Contributing

Contributing to this repository is welcome, please refer to the **Merge Request Process** above.

## Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone.

### Our Standards of Behavior

Examples of behavior that contributes to creating a positive environment
include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* Publishing others' private information, such as a physical or electronic
  address and contact details without explicit permission
* Any other conduct which could reasonably be considered inappropriate in a
  professional setting

### Our Responsibilities Regarding Code of Conduct

Project maintainers are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any contributor for other behaviors that they deem inappropriate,
threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community. Examples of
representing a project or community include using an official project e-mail
address, posting via an official social media account, or acting as an appointed
representative at an online or offline event. Representation of a project may be
further defined and clarified by project maintainers.

## Attribution

This CONTRIBUTING.md is adapted from [PurpleBooth][homepage]

[homepage]: https://gist.github.com/PurpleBooth/b24679402957c63ec426

By the time of adaption (2020-12-16), the original Code of Conduct has been 
adapted from the [Contributor Covenant][homepage], version 1.4, available 
at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/ 
